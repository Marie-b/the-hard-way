formatter = "%r %r %r %r" #inside a string. this is not a replacement.

print formatter % (1, 2, 3, 4) #the numbers are replacements
print formatter % ("one", "two", "three", "four") #the strings replace
print formatter % (True, False, False, True) #also replacements
print formatter % (formatter, formatter, formatter, formatter) #see line one
print formatter % (
     "i had this thing.",
     "that could type up right.",
     "but it didn't sing.",
     "so i said goodnight."
)     #fascninating. the "formatter" is replaced by whatever follows the percent sign. 
