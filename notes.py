m = ' multiply'
print m *4 #many times the string. can not add it to a number though
print (m+m) #one string followed by another. ok the same in this case

print "this displays a only one backslash\\"
print "this displays a just a single quote\'"
print "this displays just the double quote \""
print "this is a horizontal tab \t" #but what is that?
print "A horizontal tab is a special character that is equivalent to a number of spaces"
print "The problem is that the number of spaces depends on context"
print "A tab can be equivalent to 4 spaces:    :"
print "A tab can be equivalent to ? spaces:\t:"
print "A tab can be equivalent to 8 spaces:        :"
print "this is a vertical tab \v" #but what is that?
print "Next line after vertical tab"
print "The translation of vertical tabs is also context dependent"
print "Sometimes \\v displays as special character,\v sometimes it goes one line down"

print "this thing %s strings" % ('replaces')
print "this thing does %d" %2

hobby = raw_input ("what is your hobby\t") # you want to add trailing whitespace here; maybe a tab
print "so your hobby is %r , that is nice." % (hobby) # asks for input and replaces the %r with the raw input

print "what is your hobby?"
hobby = raw_input ()
print "so you like %r " % (hobby)  #does the same thing as above, but inserts a new line between question and input. 

print "what is your hobby?",
hobby = raw_input ()
print "so you like %r " % (hobby)  #does the same thing as above, but doesn't a new line between question and input. 

print "this neat little trick \n puts the rest in a new line"
print """ 
and the three double quotes, or alternately single quotes,
allow a string to be several lines long, and still count as one string
"""

print "\t this is displayed tabbed in"

#agrav is a argument variable. it is a 'module' or ''library' that sort of holds an argument letting you input the variables you want to use with it. example
# Orthography is aggravating.
# You can cheat by asking for a list of objects in module sys
# but you have to import the whole module
import sys
help(sys)
sysobjects= dir(sys)  # this shows all the objects (functions, lists, variables, etc.) in module sys
print sysobjects
print "Is 'agrav' in sysobjects?: %r" % ('agrav' in sysobjects)
print "Is 'argv' in sysobjects?: %r" % ('argv' in sysobjects)

from sys import argv
a, b, c = argv  # This only works if you have exactly two command line arguments
print "this is:" , a
print ", this is:" ,b 
#must enter the variable you want to use after the file name in the terminal.
#for instance "python notes.py nice real_nice" terminal prints  "this is nice, this is real_nice"
#(could be used to write little programs that calculate things if only you enter them as variables in the terminal)

#creats a prompt to ask for input and replaces the %r with that input. at this stage it looks as if i were chatting with my self. could be usefull for things like "closing program now means unsaved data is lost. do you want to close anyhow? Y/N
prompt = '>' 
print "what string to use? " , # no new line
variable = raw_input (prompt)
print "now i can replace the variable with %r like you told me to do" % (variable)

from sys import argv
script, filename = argv
txt = open(filename)
print "here is your file %r:" % filename
print txt.read() # takes a second file as input and opens/displays it.  got to open it in the python doc in the terminal and name the second file after that.  
