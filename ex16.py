from sys import argv

script, filename = argv

print "we are going to erase %r." %filename
print "if you don't want that, hit CTRL-C (^C)."
print "if you do want that, hit RETURN."

raw_input("?")

print "opening the file..."
target = open(filename, 'w')

print "truncating the file. byebye"
target.truncate()

print "now terminal will ask you three lines:"

line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")

print "terminal will write that to the file."

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

print "and now terminal closes it."
target.close()



