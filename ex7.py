print "Mary had a little lamb."
print "Its fleece was white as %s." % 'snow'
print "And everywere that Mary went."
print "." * 10 # 10 times the string or many dots

end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# without coma below, it becomes two lines
print end1 + end2 + end3 + end4 + end5 + end6,
print end7 + end8 + end9 + end10 +  end11 + end12
print "no, lamb chops"
